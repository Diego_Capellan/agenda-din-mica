package datos_IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import principal.DatosUsuario;
import principal.DatosAgenda;



public class IODatos {
	
	
	public static ArrayList <DatosAgenda> cargarContacto(String usuario) {
		ArrayList <DatosAgenda> vContactos = new ArrayList();
		
		File fichero = new File("datos/"+usuario);
		FileInputStream fi = null;
		ObjectInputStream leer = null;
		
		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
				return vContactos;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		try {
			fi = new FileInputStream(fichero);
			leer = new ObjectInputStream(fi);
			
			vContactos = (ArrayList<DatosAgenda>)leer.readObject();
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				fi.close();
				leer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return vContactos;
		
	}
	
	
	public static boolean comprobarLogin(String usuario, String cont) {
		Scanner leer = new Scanner(System.in);
		boolean contCorrecta = false;
		
		File fichero = new File("datos/usuarios.txt");
		
		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			leer = new Scanner(fichero);
			while (leer.hasNext()) {
				
				String[] linea = leer.nextLine().split("-");
				if (linea[0].equalsIgnoreCase(usuario) && linea[1].equalsIgnoreCase(cont)) {
					contCorrecta = true;
					break;
				}
				
			}	
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return contCorrecta;
		
	}
	
	
	
	public static void guardarNuevoUsuario(String usuario, String cont) {
		
		File fichero = new File("datos/usuarios.txt");
	
		FileWriter fw = null;
		PrintWriter pw = null;
		
		
		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			 fw = new FileWriter(fichero,true);
			 pw = new PrintWriter(fw);
			
			 pw.println(usuario+"-"+cont);
					
					
					
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (pw!=null)
				pw.close();
			if (fw !=null)
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}


	public static void guardarContactos(ArrayList<DatosAgenda> vContactos, String usuario) {
		
		File fichero = new File("datos/contactos.txt");
		
		FileOutputStream fo = null;
		ObjectOutputStream os = null;
		
		
		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		try {
			
			fo = new FileOutputStream(fichero);
			os = new ObjectOutputStream(fo);
			
			os.writeObject(vContactos);
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				fo.close();
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	
}
