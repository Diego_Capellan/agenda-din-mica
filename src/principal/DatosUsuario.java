package principal;

public class DatosUsuario {
	
	private String usuario;
	private String cont;
	
	
	public DatosUsuario(String usuario, String cont) {
		this.usuario = usuario;
		this.cont = cont;
	}

	
	

	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getCont() {
		return cont;
	}


	public void setCont(String cont) {
		this.cont = cont;
	}
	
}
