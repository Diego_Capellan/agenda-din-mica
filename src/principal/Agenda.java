package principal;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import datos_IO.IODatos;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;

import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.JList;
import java.awt.Window.Type;
import javax.swing.AbstractListModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Agenda extends JFrame {

	private JPanel contentPane;
	private JLabel lblAgendaTlf;
	private JLabel lblListaContactos;
	private JButton btnBuscar;
	private JTextField txrDatos;
	private JLabel lblDatos;
	private JButton btnSalir;
	private ArrayList<DatosAgenda> vContactos;
	private String usuario;
	private JList<String> listaContactos;
	private DefaultListModel<String> modeloListaContactos;
	private JTextField txtBuscar;
	private JTextField textField;
	private JLabel lblInfoBuscar;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Agenda frame = new Agenda("");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public Agenda(String usuario) {
		setType(Type.POPUP);
		this.usuario = usuario;
		vContactos = IODatos.cargarContacto(usuario);

		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 505, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblAgendaTlf = new JLabel("Agenda de Tel\u00E9fonos");
		lblAgendaTlf.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAgendaTlf.setBounds(170, 11, 172, 55);
		contentPane.add(lblAgendaTlf);

		lblListaContactos = new JLabel("Lista de Contactos:");
		lblListaContactos.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblListaContactos.setBounds(26, 80, 126, 16);
		contentPane.add(lblListaContactos);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBuscar.setBounds(304, 156, 138, 55);
		contentPane.add(btnBuscar);

		txrDatos = new JTextField();
		txrDatos.setEditable(false);
		txrDatos.setBackground(Color.WHITE);
		txrDatos.setBounds(26, 297, 436, 108);
		contentPane.add(txrDatos);
		txrDatos.setColumns(10);

		lblDatos = new JLabel("Datos Agenda:");
		lblDatos.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDatos.setBounds(192, 262, 106, 24);
		contentPane.add(lblDatos);

		btnSalir = new JButton("Salir");
		btnSalir.addMouseListener(new BtnSalirMouseListener());
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSalir.setBounds(304, 80, 138, 55);
		contentPane.add(btnSalir);

		listaContactos = new JList<String>();
		listaContactos.addMouseListener(new ListaContactosMouseListener());
		listaContactos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		listaContactos.setToolTipText("");
		modeloListaContactos = new DefaultListModel<String>();
		listaContactos.setModel(modeloListaContactos);
		
		listaContactos.setBounds(26, 107, 154, 121);
		contentPane.add(listaContactos);

		txtBuscar = new JTextField();
		txtBuscar.addKeyListener(new TxtBuscarKeyListener());
		txtBuscar.setBounds(26, 239, 126, 47);
		contentPane.add(txtBuscar);
		txtBuscar.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(26, 443, 436, 107);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblInfoBuscar = new JLabel("Datos B\u00FAsqueda:");
		lblInfoBuscar.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblInfoBuscar.setBounds(174, 416, 124, 16);
		contentPane.add(lblInfoBuscar);
		
		String txt = "";
		for (DatosAgenda d : vContactos) {
			if (d != null) {
				txt += "\n" + d.toString();
			}
		}
		txrDatos.setText(txt);
	}

	private class BtnSalirMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			
			vContactos.add(new DatosAgenda("Juan", 1234));
			IODatos.guardarContactos(vContactos, usuario);

			Login l = new Login(usuario);
			l.setVisible(true);
			dispose();

		}
	}
	

	private class TxtBuscarKeyListener extends KeyAdapter {
		@Override
		public void keyReleased(KeyEvent arg0) {
			
			String buscado = txtBuscar.getText();
			buscado+="[a-zA-Z_0-9]*";
			
			modeloListaContactos.removeAllElements();
			
			for (DatosAgenda d : vContactos) { 
				if (Pattern.matches(buscado, d.getNombreC())) { 
					modeloListaContactos.addElement(d.getNombreC());                
					}             
				}
		}
	}
	
	
	private class ListaContactosMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			
			String buscado=(String) listaContactos.getModel().getElementAt(listaContactos.getSelectedIndex());
			for (DatosAgenda d : vContactos) {
				if (d.getNombreC().equals(buscado)) {
					lblInfoBuscar.setText(d.toString());
					} 
				}
			
			
		}
	}
}