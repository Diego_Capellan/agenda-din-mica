package principal;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import datos_IO.IODatos;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldUsuario;
	private JButton btnLogin;
	private JLabel lblUsuario;
	private JLabel lblContraseña;
	private JPasswordField passContraseña;
	private JButton btnRegistro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login("");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login(String usuario) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 437);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.setBounds(186, 69, 122, 32);
		contentPane.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);
		
		btnLogin = new JButton("Login");
		btnLogin.addMouseListener(new BtnLoginMouseListener());
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnLogin.setBounds(172, 210, 148, 54);
		contentPane.add(btnLogin);
		
		lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUsuario.setBounds(219, 43, 54, 15);
		contentPane.add(lblUsuario);
		
		lblContraseña = new JLabel("Contrase\u00F1a");
		lblContraseña.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblContraseña.setBounds(204, 125, 85, 14);
		contentPane.add(lblContraseña);
		
		passContraseña = new JPasswordField();
		passContraseña.setEchoChar('*');
		passContraseña.setBounds(186, 150, 122, 32);
		contentPane.add(passContraseña);
		
		btnRegistro = new JButton("Registro");
		btnRegistro.addMouseListener(new BtnRegistroMouseListener());
		btnRegistro.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnRegistro.setBounds(172, 291, 148, 54);
		contentPane.add(btnRegistro);
	}
	
	private class BtnLoginMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			
			if (IODatos.comprobarLogin(textFieldUsuario.getText(), passContraseña.getText()) == true) {
				Agenda a = new Agenda(textFieldUsuario.getText());
				a.setVisible(true);
				dispose();
				
			}else {
				JOptionPane.showMessageDialog(rootPane, "Has Introducido Mal los Datos o el Usuario Introducido no Existe", "Inicio de Sesión", 0);
				
			}
		}
	}
	
	
	private class BtnRegistroMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			Registrar r = new Registrar();
			r.setVisible(true);
			dispose();
			
		}
	}
	
	
	
}
