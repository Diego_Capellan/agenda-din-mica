package principal;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import datos_IO.IODatos;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;
import javax.swing.JEditorPane;

public class Registrar extends JFrame {

	private JPanel contentPane;
	private JButton btnRegistrar;
	private JLabel lblNuevoUsuario;
	private JLabel lblNuevaContraseña;
	private JTextField textFieldNuevoUsuario;
	private JPasswordField passNuevaContraseña;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registrar frame = new Registrar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registrar() {
		
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 521, 410);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addMouseListener(new BtnRegistrarMouseListener());
		btnRegistrar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRegistrar.setBounds(175, 245, 142, 57);
		contentPane.add(btnRegistrar);
		
		lblNuevoUsuario = new JLabel("Usuario");
		lblNuevoUsuario.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNuevoUsuario.setBounds(221, 64, 46, 14);
		contentPane.add(lblNuevoUsuario);
		
		lblNuevaContraseña = new JLabel("Contrase\u00F1a");
		lblNuevaContraseña.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNuevaContraseña.setBounds(211, 147, 86, 14);
		contentPane.add(lblNuevaContraseña);
		
		textFieldNuevoUsuario = new JTextField();
		textFieldNuevoUsuario.setBounds(191, 89, 106, 34);
		contentPane.add(textFieldNuevoUsuario);
		textFieldNuevoUsuario.setColumns(10);
		
		passNuevaContraseña = new JPasswordField();
		passNuevaContraseña.setEchoChar('*');
		passNuevaContraseña.setBounds(191, 181, 106, 34);
		contentPane.add(passNuevaContraseña);
	}

	
	private class BtnRegistrarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			IODatos.guardarNuevoUsuario(textFieldNuevoUsuario.getText(), passNuevaContraseña.getText());
			
			Login l = new Login(textFieldNuevoUsuario.getText());
			l.setVisible(true);
			dispose();
			
			
		}
	}
}
