package principal;

import java.io.Serializable;

public class DatosAgenda implements Serializable{

	private String nombreC;
	private int numeroTlf;
	
	
	public DatosAgenda(String nombreC, int numeroTlf) {
		this.nombreC = nombreC;
		this.numeroTlf = numeroTlf;
	}

	
	
	
	public String toString() {
		return "Nombre Contacto: " + nombreC + " N�mero de Tel�fono:" + numeroTlf;
	}




	public String getNombreC() {
		return nombreC;
	}


	public void setNombreC(String nombreC) {
		this.nombreC = nombreC;
	}


	public int getNumeroTlf() {
		return numeroTlf;
	}


	public void setNumeroTlf(int numeroTlf) {
		this.numeroTlf = numeroTlf;
	}
	
}
